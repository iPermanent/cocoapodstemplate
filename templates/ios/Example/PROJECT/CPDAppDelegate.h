//
//  CPDAppDelegate.h
//  PROJECT
//
//  Created by zhangheng on TODAYS_DATE.
//  Copyright (c) TODAYS_YEAR zhangheng. All rights reserved.
//

@import UIKit;

@interface CPDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
